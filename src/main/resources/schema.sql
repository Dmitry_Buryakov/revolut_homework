create sequence account_seq;
create sequence transfer_log_seq;

create table account(
    number varchar(100) primary key,
    date_start timestamp not null,
    date_end timestamp null,
    balance numeric(20, 2) default 0 not null
);

create table transfer_log(
    id bigint default nextval('transfer_log_seq') primary key,
    date_registered timestamp not null,
    from_account_fk varchar(100) not null,
    to_account_fk varchar(100) not null,
    amount numeric(20, 2) not null,
    successful boolean not null,
    commentary text,
    constraint from_account_fc FOREIGN KEY (from_account_fk) REFERENCES account(number),
    constraint to_account_fc FOREIGN KEY (to_account_fk) REFERENCES account(number)
);