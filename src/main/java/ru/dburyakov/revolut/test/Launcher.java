package ru.dburyakov.revolut.test;

import com.google.inject.Guice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.di.DiContext;
import ru.dburyakov.revolut.test.di.ProductionDiModule;
import ru.dburyakov.revolut.test.http.HttpServer;

public class Launcher {

    private static Logger log = LoggerFactory.getLogger(Launcher.class);


    public static void main(String[] args) {
        log.info("initializing DI...");
        DiContext.setInjector(Guice.createInjector(new ProductionDiModule()));
        log.info("DI have been initialized");

        log.info("populating data ...");
        DataPopulationUtils dataPopulator = DiContext.getBean(DataPopulationUtils.class);
        dataPopulator.createSchema();
        dataPopulator.populateData();
        log.info("data have been populated ...");


        HttpServer httpServer = DiContext.getBean(HttpServer.class);
        httpServer.start(5, 20);

    }
}
