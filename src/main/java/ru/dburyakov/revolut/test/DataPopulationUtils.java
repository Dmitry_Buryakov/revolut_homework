package ru.dburyakov.revolut.test;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.Optional;
import java.util.stream.Collectors;

public class DataPopulationUtils {

    @Inject
    private DataSource ds;

    public void createSchema() {
        executeSqlResource("/schema.sql");
    }

    public void populateData() {
        executeSqlResource("/data.sql");
    }

    private void executeSqlResource(String path) {
        Optional.ofNullable(this.getClass().getResourceAsStream(path)).ifPresent(sqlStream -> {
            try(
                    Connection connection = ds.getConnection();
                    BufferedReader sqlReader = new BufferedReader(new InputStreamReader(sqlStream))
            ) {
                String sql = sqlReader.lines().collect(Collectors.joining("\n"));
                System.out.println(sql);
                connection.createStatement().execute(sql);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
