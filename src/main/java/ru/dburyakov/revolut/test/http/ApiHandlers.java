package ru.dburyakov.revolut.test.http;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.domain.Account;
import ru.dburyakov.revolut.test.domain.TransferLog;
import ru.dburyakov.revolut.test.http.dto.TransferCommand;
import ru.dburyakov.revolut.test.service.AccountService;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionException;

class ApiHandlers {

    private Logger log = LoggerFactory.getLogger(getClass());

    @FunctionalInterface
    private interface WorkerAction{
        void run() throws Exception;
    }

    private class JsonExceptionWrapper {
        private String error;

        public JsonExceptionWrapper(Exception error) {
            Throwable prev = error;
            while(prev.getCause() != null) {
                prev = prev.getCause();
            }
            this.error = prev.getMessage();
        }

        public String getError() {
            return error;
        }
    }

    @Inject
    private ObjectMapper objectMapper;
    @Inject
    private AccountService accountService;

    void getAccounts(HttpServerExchange ctx) {
        async(ctx, () -> {
            List<Account> accs = accountService.findAll().join(); //we already in async context of undertow thread pool
            objectMapper.writeValue(ctx.getOutputStream(), accs);
        });
    }

    void getAccountByNumber(HttpServerExchange ctx) {
        async(ctx, () -> {
            String numberParam = Optional.ofNullable(ctx.getQueryParameters().get("number")).map(Deque::getFirst).orElse("");
            Optional<Account> acc = accountService.findByNumber(numberParam).join(); //we already in async context of undertow thread pool
            if (acc.isEmpty()) {
                ctx.setStatusCode(StatusCodes.NOT_FOUND);
            } else {
                objectMapper.writeValue(ctx.getOutputStream(), acc);
            }
        });
    }

    void transferMoney(HttpServerExchange ctx) {
        async(ctx, () -> {
            TransferCommand cmd = objectMapper.readValue(ctx.getInputStream(), TransferCommand.class);
            TransferLog txLog = accountService.transfer(cmd.getFromAccount(), cmd.getToAccount(), cmd.getAmount()).join(); //we already in async context of undertow thread pool
            objectMapper.writeValue(ctx.getOutputStream(), txLog);
        });
    }

    private void async(HttpServerExchange ctx, WorkerAction action) {
        ctx.getResponseHeaders().add(Headers.CONTENT_TYPE, "application/json");
        ctx.dispatch(() -> {
            ctx.startBlocking();
            try {
                action.run();
            } catch (AccountService.TransferException | CompletionException | JsonParseException e) {
                log.error("", e);
                ctx.setStatusCode(StatusCodes.BAD_REQUEST);
                try {
                    objectMapper.writeValue(ctx.getOutputStream(), new JsonExceptionWrapper(e));
                } catch (IOException ex) {
                    log.error("", ex);
                    ctx.setStatusCode(StatusCodes.INTERNAL_SERVER_ERROR);
                }
            } catch (Exception e) {
                log.error("", e);
                ctx.setStatusCode(StatusCodes.INTERNAL_SERVER_ERROR);
            } finally {
                ctx.endExchange();
            }
        });
    }
}
