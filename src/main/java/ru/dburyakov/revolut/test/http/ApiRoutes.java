package ru.dburyakov.revolut.test.http;

import io.undertow.server.RoutingHandler;

import javax.inject.Inject;

public class ApiRoutes extends RoutingHandler{

    @Inject
    public ApiRoutes(ApiHandlers handlers) {
        get("/api/v1/accounts", handlers::getAccounts);
        get("/api/v1/account/{number}", handlers::getAccountByNumber);
        post("/api/v1/transfer", handlers::transferMoney);
    }



}