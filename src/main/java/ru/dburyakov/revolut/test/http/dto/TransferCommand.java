package ru.dburyakov.revolut.test.http.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class TransferCommand {

    private String fromAccount;
    private String toAccount;
    private BigDecimal amount;

    @JsonCreator
    public TransferCommand(
            @JsonProperty("fromAccount")  String fromAccount,
            @JsonProperty("toAccount") String toAccount,
            @JsonProperty("amount") BigDecimal amount) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
