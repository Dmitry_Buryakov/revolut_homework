package ru.dburyakov.revolut.test.http;

import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Optional;

public class HttpServer {

    @Inject
    private RoutingHandler routes;
    private Undertow server;
    private String host = "0.0.0.0";
    private int port = 9000;

    private Logger log = LoggerFactory.getLogger(getClass());

    public void start(int ioThreadsCount, int workerThreadCount) {
        log.info("starting http server on {}:{}", host, port);
        Undertow.Builder builder = Undertow.builder();
        builder.setIoThreads(ioThreadsCount);
        builder.setWorkerThreads(workerThreadCount);
        builder.addHttpListener(port, host);
        builder.setHandler(routes);
        server = builder.build();
        server.start();
        log.debug("HTTP server started");
    }

    public void stop() {
        Optional.ofNullable(server).ifPresent(Undertow::stop);
    }
}
