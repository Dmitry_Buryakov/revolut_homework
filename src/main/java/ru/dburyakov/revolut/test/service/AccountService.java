package ru.dburyakov.revolut.test.service;

import org.jooq.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.DB;
import ru.dburyakov.revolut.test.db.tables.records.AccountRecord;
import ru.dburyakov.revolut.test.domain.Account;
import ru.dburyakov.revolut.test.domain.TransferLog;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static ru.dburyakov.revolut.test.db.tables.Account.ACCOUNT;
import static ru.dburyakov.revolut.test.db.tables.TransferLog.TRANSFER_LOG;

public class AccountService {

    private Logger log = LoggerFactory.getLogger(getClass());

    public class TransferException extends RuntimeException {
        public TransferException(String message) {
            super(message);
        }
    }

    public CompletableFuture<List<Account>> findAll() {
        return DB.exec(true, dsl ->
            dsl.select()
            .from(ACCOUNT)
            .orderBy(ACCOUNT.NUMBER)
            .fetchInto(Account.class)
        );
    }

    public CompletableFuture<Optional<Account>> findByNumber(String accNumber) {
        return DB.exec(true, dsl ->
            Optional.ofNullable(
                dsl.select()
                .from(ACCOUNT)
                .where(ACCOUNT.NUMBER.eq(accNumber))
                .fetchOneInto(Account.class)
            )
        );
    }

    public CompletableFuture<TransferLog> transfer(String from, String to, BigDecimal amount) {

        if (from == null || to == null || from.isBlank() || to.isBlank()) {
            return CompletableFuture.failedFuture(new TransferException("invalid account"));
        } else if (from.trim().equals(to.trim())) {
            return CompletableFuture.failedFuture(new TransferException("transfer to the same account"));
        } else if (amount == null || amount.compareTo(BigDecimal.ZERO) < 0) {
            return CompletableFuture.failedFuture(new TransferException("invalid amount"));
        }

        return DB.txAsync(dsl -> {
            LocalDateTime txDate = LocalDateTime.now();
            if (!dsl.fetchExists(ACCOUNT, ACCOUNT.NUMBER.eq(from.trim()).and(ACCOUNT.DATE_END.isNull().or(ACCOUNT.DATE_END.gt(txDate))))) {
                throw new TransferException(String.format("account %s doesn't exists or closed", from));
            }
            if (!dsl.fetchExists(ACCOUNT, ACCOUNT.NUMBER.eq(to.trim()).and(ACCOUNT.DATE_END.isNull().or(ACCOUNT.DATE_END.gt(txDate))))) {
                throw new TransferException(String.format("account %s doesn't exists or closed", to));
            }

            try{
                synchronized (this) {
                    Account.lock(from);
                    log.info("lock {}", from);
                    Account.lock(to);
                    log.info("lock {}", to);
                }

                Record fromRecord = dsl.select().from(ACCOUNT).where(ACCOUNT.NUMBER.eq(from.trim())).fetchOne();
                Record toRecord = dsl.select().from(ACCOUNT).where(ACCOUNT.NUMBER.eq(to.trim())).fetchOne();

                Account withdrawAccount = fromRecord.into(Account.class);
                Account chargeAccount = toRecord.into(Account.class);

                if (withdrawAccount.getBalance().compareTo(amount) > 0) {
                    withdrawAccount.withdraw(amount);
                    chargeAccount.charge(amount);
                    TransferLog txLog = new TransferLog(null, txDate, withdrawAccount.getNumber(), chargeAccount.getNumber(), amount, true, "OK");

                    dsl.newRecord(TRANSFER_LOG, txLog).store();
                    ((AccountRecord) fromRecord).setBalance(withdrawAccount.getBalance()).store();
                    ((AccountRecord) toRecord).setBalance(chargeAccount.getBalance()).store();
                    return txLog;
                } else {
                    throw new TransferException(String.format("account %s balance less then %s", from, amount.toString()));
                }
            } finally {
                Account.unlock(from);
                log.info("unlock {}", from);
                Account.unlock(to);
                log.info("unlock {}", to);
            }

        });
    }

}
