package ru.dburyakov.revolut.test.di;

import com.google.inject.Injector;

public class DiContext {

    private static Injector injector;

    private DiContext(){
    }

    public static void setInjector(Injector injector) {
        DiContext.injector = injector;
    }

    public static <T> T getBean(Class<T> ofClass) {
        if (injector == null) throw new RuntimeException("injector hasn't been set yet");
        return injector.getInstance(ofClass);
    }

}
