package ru.dburyakov.revolut.test.di;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.AbstractModule;
import com.zaxxer.hikari.HikariDataSource;
import io.undertow.server.RoutingHandler;
import ru.dburyakov.revolut.test.http.ApiRoutes;

import javax.sql.DataSource;
import java.text.DateFormat;

public class ProductionDiModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();
        this.bind(RoutingHandler.class).to(ApiRoutes.class);
        this.bind(ObjectMapper.class).toInstance(objectMapper());
        this.bind(DataSource.class).toInstance(dataSource());
    }

    protected ObjectMapper objectMapper() {
        return new ObjectMapper()
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule())
                .setDateFormat(DateFormat.getDateTimeInstance());
    }

    protected DataSource dataSource() {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:h2:mem:revolut;DB_CLOSE_DELAY=-1");
        ds.setUsername("sa");
        ds.setPassword("");
        ds.setDriverClassName("org.h2.Driver");
        ds.setMinimumIdle(1);
        ds.setMaximumPoolSize(10);
        ds.setConnectionTimeout(30000);
        ds.setMaxLifetime(30000);
        return ds;
    }
}
