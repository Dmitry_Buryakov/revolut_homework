package ru.dburyakov.revolut.test;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.di.DiContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public class DB {

    private static Logger log = LoggerFactory.getLogger(DB.class);
    //private static ExecutorService executor = Executors.newFixedThreadPool(10);

    @FunctionalInterface
    public interface DbAction<T> {
        T action(DSLContext jooqCtx) throws Exception;
    }


    public static <T> CompletableFuture<T> txAsync(DbAction<T> action) {
        return CompletableFuture.supplyAsync(() -> {
            try (Connection connection = DiContext.getBean(DataSource.class).getConnection()) {
                DSLContext dsl = DSL.using(connection);
                return dsl.transactionResult(tx -> action.action(dsl));
            } catch (SQLException e) {
                log.error("transaction error", e);
                throw new CompletionException(e);
            }
        });
    }

    public static <T> CompletableFuture<T> exec(boolean ro, DbAction action) {
        return CompletableFuture.supplyAsync(() -> {
            try (Connection connection = DiContext.getBean(DataSource.class).getConnection()) {
                connection.setReadOnly(ro);
                DSLContext jooqCtx = DSL.using(connection);
                return (T) action.action(jooqCtx);
            } catch (Exception e) {
                log.error("transaction error", e);
                throw new CompletionException(e);
            }
        });
    }
}
