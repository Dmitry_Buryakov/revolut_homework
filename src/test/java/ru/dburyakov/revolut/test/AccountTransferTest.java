package ru.dburyakov.revolut.test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.di.DiContext;
import ru.dburyakov.revolut.test.domain.Account;
import ru.dburyakov.revolut.test.domain.TransferLog;
import ru.dburyakov.revolut.test.service.AccountService;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({InitializationExtension.class})
public class AccountTransferTest {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Test
    @DisplayName("correct transfer")
    public void testTransferCorrect() {
        String withdrawAccountName = "test-acc-3";
        String chargeAccountName = "test-acc-1";
        BigDecimal amount = BigDecimal.valueOf(1000.50);

        AccountService accService = DiContext.getBean(AccountService.class);

        Account withdrawAccountBeforeOperation = accService.findByNumber(withdrawAccountName).join().get();
        Account chargeAccountBeforeOperation = accService.findByNumber(chargeAccountName).join().get();

        TransferLog txLog = accService.transfer(withdrawAccountName, chargeAccountName, amount).join();

        Account withdrawAccountAfterOperation = accService.findByNumber(withdrawAccountName).join().get();
        Account chargeAccountAfterOperation = accService.findByNumber(chargeAccountName).join().get();

        assertEquals(withdrawAccountBeforeOperation.getBalance().subtract(amount), withdrawAccountAfterOperation.getBalance());
        assertEquals(chargeAccountBeforeOperation.getBalance().add(amount), chargeAccountAfterOperation.getBalance());
        assertEquals(txLog.getFromAccountFk(), withdrawAccountAfterOperation.getNumber());
        assertEquals(txLog.getToAccountFk(), chargeAccountAfterOperation.getNumber());
        assertEquals(txLog.getAmount(), amount);
        assertNotNull(txLog.getDateRegistered());
        assertTrue(txLog.getSuccessful());
    }


    @Test
    @DisplayName("transfer on the same account")
    public void testTransferOnSameAccount() {
        String withdrawAccountName = "test-acc-3";
        String chargeAccountName = "test-acc-3";
        BigDecimal amount = BigDecimal.valueOf(1000.50);

        CompletionException exception = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer(withdrawAccountName, chargeAccountName, amount).join();
        });

        assertEquals("transfer to the same account", exception.getCause().getMessage());
    }

    @Test
    @DisplayName("transfer on invalid account")
    public void testTransferOnInvalidAccount() {
        BigDecimal amount = BigDecimal.valueOf(1000.50);

        CompletionException exception1 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("test-acc-3", "", amount).join();
        });

        CompletionException exception2 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("", "test-acc-3", amount).join();
        });

        CompletionException exception3 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("", "", amount).join();
        });

        assertEquals("invalid account", exception1.getCause().getMessage());
        assertEquals("invalid account", exception2.getCause().getMessage());
        assertEquals("invalid account", exception3.getCause().getMessage());
    }

    @Test
    @DisplayName("transfer between not existing accounts")
    public void testTransferNotExistingAccountAccount() {
        BigDecimal amount = BigDecimal.valueOf(1000.50);

        CompletionException exception1 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("test-acc-0", "test-acc-3", amount).join();
        });

        CompletionException exception2 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("test-acc-3", "test-acc-0", amount).join();
        });

        assertEquals("account test-acc-0 doesn't exists or closed", exception1.getCause().getMessage());
        assertEquals("account test-acc-0 doesn't exists or closed", exception2.getCause().getMessage());
    }

    @Test
    @DisplayName("transfer invalid amount")
    public void testTransferInvalidAmount() {
        BigDecimal amount = BigDecimal.valueOf(-1000.50);

        CompletionException exception1 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("test-acc-3", "test-acc-1", null).join();
        });

        CompletionException exception2 = assertThrows(CompletionException.class, () -> {
            AccountService accService = DiContext.getBean(AccountService.class);
            accService.transfer("test-acc-3", "test-acc-1", amount).join();
        });


        assertEquals("invalid amount", exception1.getCause().getMessage());
        assertEquals("invalid amount", exception2.getCause().getMessage());
    }

    @Test
    @DisplayName("transfer too much amount")
    public void testTransferTooMuch() {

        AccountService accService = DiContext.getBean(AccountService.class);
        Account withdrawAccountBeforeOperation = accService.findByNumber("test-acc-3").join().get();
        assertTrue(withdrawAccountBeforeOperation.getBalance().compareTo(BigDecimal.ONE) > 0);
        BigDecimal amount = BigDecimal.valueOf(1000.50).add(withdrawAccountBeforeOperation.getBalance());

        CompletionException exception1 = assertThrows(CompletionException.class, () -> {
            accService.transfer("test-acc-3", "test-acc-1", amount).join();
        });

        assertEquals("account test-acc-3 balance less then " + amount.toString(), exception1.getCause().getMessage());
    }


    @RepeatedTest(10)
    @DisplayName("parallel transfers")
    public void testParallel() {
        String withdrawAccountName = "test-acc-4";
        String chargeAccountName = "test-acc-2";
        AccountService accService = DiContext.getBean(AccountService.class);

        CompletableFuture<TransferLog> f1 = accService.transfer(
            withdrawAccountName,
            chargeAccountName,
            BigDecimal.valueOf(100.0)
        );

        CompletableFuture<TransferLog> f2 = accService.transfer(
            chargeAccountName,
            withdrawAccountName,
            BigDecimal.valueOf(100.0)
        );

        CompletableFuture.allOf(f1, f2).exceptionally(Assertions::fail).orTimeout(5, TimeUnit.SECONDS).join();
    }
}
