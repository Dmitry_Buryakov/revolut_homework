package ru.dburyakov.revolut.test;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.di.DiContext;
import ru.dburyakov.revolut.test.http.dto.TransferCommand;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith({InitializationExtension.class})
public class HttpResponsesTest {

    private String baseUrl = "http://localhost:9000/api/v1";
    private HttpClient client = HttpClient.newHttpClient();

    private Logger log = LoggerFactory.getLogger(getClass());

    @Test
    @DisplayName("get account list")
    public void testGetAccountList() throws IOException, InterruptedException {

        ObjectMapper objectMapper = DiContext.getBean(ObjectMapper.class);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl + "/accounts"))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(200, response.statusCode());

        log.info(response.body());

        JsonNode parsedJson = objectMapper.readTree(response.body());
        assertTrue(parsedJson.isArray());
        assertTrue(parsedJson.size() > 0);
    }

    @Test
    @DisplayName("get one account")
    public void testGetOneAccount() throws IOException, InterruptedException {

        ObjectMapper objectMapper = DiContext.getBean(ObjectMapper.class);
        String testAccount = "test-acc-1";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl + "/account/" + testAccount))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(200, response.statusCode());

        log.info(response.body());

        JsonNode parsedJson = objectMapper.readTree(response.body());
        assertTrue(parsedJson.isObject());
        assertEquals(testAccount, parsedJson.get("number").asText());
    }

    @Test
    @DisplayName("get one account")
    public void testGetNotExistingAccount() throws IOException, InterruptedException {

        String testAccount = "test-acc";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl + "/account/" + testAccount))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(404, response.statusCode());
    }

    @Test
    @DisplayName("post transfer transaction")
    public void testTransfer() throws IOException, InterruptedException {
        ObjectMapper objectMapper = DiContext.getBean(ObjectMapper.class);

        TransferCommand cmd = new TransferCommand("test-acc-2", "test-acc-1", BigDecimal.valueOf(10));
        String requestJson = objectMapper.writeValueAsString(cmd);

        HttpRequest request = HttpRequest.newBuilder()
                .method("POST", HttpRequest.BodyPublishers.ofString(requestJson))
                .uri(URI.create(baseUrl + "/transfer"))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(200, response.statusCode());

        JsonNode parsedJson = objectMapper.readTree(response.body());
        assertTrue(parsedJson.isObject());
        assertEquals("test-acc-2", parsedJson.get("fromAccountFk").asText());
        assertEquals("test-acc-1", parsedJson.get("toAccountFk").asText());
        assertEquals(10.0, parsedJson.get("amount").asDouble());
    }

    @Test
    @DisplayName("post invalid transfer transaction")
    public void testInvalidTransfer() throws IOException, InterruptedException {
        ObjectMapper objectMapper = DiContext.getBean(ObjectMapper.class);

        TransferCommand cmd = new TransferCommand("test-acc", "test-acc-1", BigDecimal.valueOf(10));
        String requestJson = objectMapper.writeValueAsString(cmd);

        HttpRequest request = HttpRequest.newBuilder()
                .method("POST", HttpRequest.BodyPublishers.ofString(requestJson))
                .uri(URI.create(baseUrl + "/transfer"))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        assertEquals(400, response.statusCode());

        log.info(response.body());

        JsonNode parsedJson = objectMapper.readTree(response.body());
        assertTrue(parsedJson.isObject());
        assertEquals("account test-acc doesn't exists or closed", parsedJson.get("error").asText());
    }

}
