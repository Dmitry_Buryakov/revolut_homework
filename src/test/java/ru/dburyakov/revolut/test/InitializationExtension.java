package ru.dburyakov.revolut.test;

import com.google.inject.Guice;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dburyakov.revolut.test.di.DiContext;
import ru.dburyakov.revolut.test.http.HttpServer;

import java.util.concurrent.atomic.AtomicBoolean;

public class InitializationExtension implements BeforeAllCallback, AfterAllCallback {

    private static AtomicBoolean initialized = new AtomicBoolean(false);
    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void beforeAll(ExtensionContext context) {
        synchronized (this) {
            if (!initialized.get()) {
                log.info("initializing DI...");
                DiContext.setInjector(Guice.createInjector(new TestDiModule()));
                log.info("DI have been initialized");

                log.info("populating data ...");
                DataPopulationUtils dataPopulator = DiContext.getBean(DataPopulationUtils.class);
                dataPopulator.createSchema();
                dataPopulator.populateData();
                log.info("data have been populated ...");


                HttpServer httpServer = DiContext.getBean(HttpServer.class);
                httpServer.start(5, 20);
                initialized.set(true);
            }
        }
    }

    @Override
    public void afterAll(ExtensionContext context) {
        DiContext.getBean(HttpServer.class).stop();
    }
}
