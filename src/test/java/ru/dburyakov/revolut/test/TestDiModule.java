package ru.dburyakov.revolut.test;

import com.zaxxer.hikari.HikariDataSource;
import ru.dburyakov.revolut.test.di.ProductionDiModule;

import javax.sql.DataSource;

public class TestDiModule extends ProductionDiModule {

    @Override
    protected DataSource dataSource() {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:h2:mem:revolut_test_db;DB_CLOSE_DELAY=-1");
        ds.setUsername("sa");
        ds.setPassword("");
        ds.setDriverClassName("org.h2.Driver");
        ds.setMinimumIdle(1);
        ds.setMaximumPoolSize(5);
        ds.setConnectionTimeout(30000);
        ds.setMaxLifetime(30000);
        return ds;
    }
}
